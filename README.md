# LaraBlog

Simple Blog based on Laravel 10 + Vue 3 + Inertia.

## Project requirements

- PHP 8.1.0
- Composer 2.2.0
 - Docker

## Getting started

1. Clone the project
2. Move into project root and install all dependencies by running next commands
```
composer install
npm install
```
3. Build and run Docker containers for MySQL, MailHog, Adminer
```
docker-compose build
docker-compose up -d
```
4. Create .env file by copying and renaming .env.example file at hte root and provide GitLab client id, client secret and callback uri to .env file to let LaraBlog register and login users via GitLab.
5. Run migrations with DB seeding
```
php artisan migrate --seed
```
6. Create symlink for storage
```
php artisan storage:link
```
7. To start project run built-in Laravel server and Vite. (These commands must work at the same time, so you need two terminals)
```
php artisan serve
npm run dev
```
8. Now site should be available at http://127.0.0.1:8000/
9. Adminer should be available at http://localhost:8080/?server=mysql with next credentials
```
Server: mysql
Login: root
Password: root
```
10. MailHog should be available at http://localhost:8025/
