<?php

use Illuminate\Support\Facades\Route;
use \App\Http\Controllers\IndexController;
use Illuminate\Foundation\Auth\EmailVerificationRequest;
use Illuminate\Http\Request;


/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "web" middleware group. Make something great!
|
*/

Route::get('/', [IndexController::class, 'index']);

Route::get('/home', [IndexController::class, 'index'])
    ->name('home');

Route::middleware('guest')->group(function () {
    Route::get('auth', [\App\Http\Controllers\AuthController::class, 'index'])
        ->name('login');
    Route::post('login', [\App\Http\Controllers\AuthController::class, 'store'])
        ->name('login.store');
});

Route::delete('logout', [\App\Http\Controllers\AuthController::class, 'destroy'])
    ->middleware('auth')
    ->name('logout');

Route::get('/email/verify', function () {
    return inertia('Auth/VerifyEmail');
})->middleware('auth')->name('verification.notice');

Route::get('/email/verify/{id}/{hash}', function (EmailVerificationRequest $request) {
    $request->fulfill();
    return redirect()->route('home')->with('success', 'Email was verified!');
})->middleware(['auth', 'signed'])->name('verification.verify');

Route::post('/email/verification-notification', function (Request $request) {
    $request->user()->sendEmailVerificationNotification();
    return back()->with('success', 'Verification link sent!');
})->middleware(['auth', 'throttle:6,1'])->name('verification.send');

Route::resource('user-account', \App\Http\Controllers\UserAccountController::class)
    ->middleware(['auth'])
    ->only(['index', 'update']);

Route::resource('user-account', \App\Http\Controllers\UserAccountController::class)
    ->only(['store']);

Route::resource('post', \App\Http\Controllers\PostController::class)
    ->middleware(['auth'])
    ->only(['create', 'update', 'store', 'destroy', 'edit']);

Route::resource('post', \App\Http\Controllers\PostController::class)
    ->only(['index', 'show']);

Route::resource('tag', \App\Http\Controllers\TagController::class);

Route::resource('user-account.avatar', \App\Http\Controllers\UserAccountAvatarController::class)
    ->only(['store']);

Route::delete('/user-account/{user_account}/destroy', [\App\Http\Controllers\UserAccountAvatarController::class, 'destroy'])
    ->name('user-account.avatar.destroy');

Route::get('blog', [\App\Http\Controllers\BlogController::class, 'index'])
    ->name('blog');

Route::get('/gitlab_callback', [\App\Http\Controllers\GitlabController::class, 'callback'])->middleware('guest');

Route::prefix('password')
    ->name('password.')
    ->middleware('guest')
    ->group(function () {
        Route::get('/forgot-password', [\App\Http\Controllers\ForgotPasswordController::class, 'create'])
            ->name('request');
        Route::post('/forgot-password', [\App\Http\Controllers\ForgotPasswordController::class, 'store'])
            ->name('email');
        Route::get('/reset-password/{token}', [\App\Http\Controllers\ResetPasswordController::class, 'create'])
            ->name('reset');
        Route::post('/reset-password', [\App\Http\Controllers\ResetPasswordController::class, 'store'])
            ->name('update');
    });
