<?php

namespace App\Http\Controllers;

use App\Models\Post;
use Illuminate\Http\Request;
use Inertia\Inertia;

class IndexController extends Controller
{
    public function index()
    {
        return Inertia::render('Index/Index',
            [
                'posts' => Post::mostRecent()
                    ->with(['author', 'tags'])
                    ->take(9)
                    ->get()
            ]);
    }
}
