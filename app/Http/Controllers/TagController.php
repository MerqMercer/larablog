<?php

namespace App\Http\Controllers;

use App\Models\Post;
use Illuminate\Http\Request;
use App\Models\Tag;
use Inertia\Inertia;
use Illuminate\Database\Eloquent\Builder;

class TagController extends Controller
{
    public function show(Tag $tag, Request $request)
    {
        $sort = $request->only(['created_at']);
        $posts = Post::sort($sort)
            ->with('author', 'tags')
            ->whereHas('tags', function (Builder $query) use ($tag) {
                $query->where('tags.id', $tag->id);
            })->paginate(9);

        return Inertia::render('Tag/Show', ['tag' => $tag, 'posts' => $posts]);
    }
}
