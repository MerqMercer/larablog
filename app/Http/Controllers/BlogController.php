<?php

namespace App\Http\Controllers;

use App\Models\Post;
use Illuminate\Http\Request;
use Inertia\Inertia;

class BlogController extends Controller
{
    public function index(Request $request)
    {
        $sort = $request->only(['created_at']);
        return Inertia::render('Blog/Index',
            [
                'posts' => Post::sort($sort)
                    ->with(['author', 'tags'])
                    ->paginate(9)
                    ->withQueryString()
            ]);
    }
}
