<?php

namespace App\Http\Controllers;

use App\Models\Post;
use App\Models\User;
use Illuminate\Auth\Events\Registered;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Validation\Rule;
use Inertia\Inertia;

class UserAccountController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index(Post $post)
    {
        $currentUsersPosts = !Auth::user() ? null : $post->byMe()->with('author','tags')->paginate(9);
        return Inertia::render(
            'UserAccount/Index',
            [
                'currentUser' => Auth::user(),
                'currentUsersPosts' => $currentUsersPosts
            ]);
    }


    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request)
    {
        $user = User::create($request->validate([
            'name' => 'required',
            'email' => 'required|email|unique:users',
            'password' => 'required|min:8|confirmed'
        ]));

        Auth::login($user);
        event(new Registered($user));

        return redirect()->route('user-account.index')
            ->with('success', 'Account created!');
    }

    public function update(Request $request, User $user_account)
    {
        $user_account->update(
            $request->validate([
                'name' => 'min:2|max:100',
                'nickname' => [
                    'required',
                    'min:5',
                    'max:25',
                    Rule::unique('users')->ignore($user_account->id),
                ],
                'email' => [
                    'required',
                    'email',
                    Rule::unique('users')->ignore($user_account->id),
                ],
                'birthday' => 'required',
                //'address' => 'required|min:8|max:50'
            ])
        );

        return redirect()->route('user-account.index')
            ->with('success', 'Account was updated!');
    }

}
