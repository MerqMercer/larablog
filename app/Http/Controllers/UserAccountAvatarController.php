<?php

namespace App\Http\Controllers;

use App\Models\User;
use Illuminate\Support\Facades\Storage;
use Illuminate\Http\Request;

class UserAccountAvatarController extends Controller
{
    public function store(User $user_account, Request $request)
    {

        if ($request->hasFile('image')) {
            $request->validate([
                'image' => 'required|mimes:jpg,png,jpeg,webp|max:5000'
            ], [
                'image.mimes' => 'The file should be in one of the formats: jpg, png, jpeg, webp'
            ]);

            if ($user_account->avatar_path) {
                Storage::disk('public')->delete($user_account->avatar_path);
            }

            $path = $request->file('image')->store('avatars/'.$user_account->id, 'public');

            if ($path) {
                $user_account->update(['avatar_path' => $path]);

                return redirect()->route('user-account.index')
                    ->with('success', 'Avatar Updated!');
            }
        }

        return redirect()->route('user-account.index')
            ->with('error', 'Avatar was not Updated!');
    }

    public function destroy(User $user_account)
    {
        if (!$user_account->avatar_path) {
            return redirect()->route('user-account.index')
                ->with('error', 'Avatar is not found!');
        }

        Storage::disk('public')->delete($user_account->avatar_path);
        $user_account->update(['avatar_path' => null]);

        return redirect()->back()->with('success', 'Image was deleted!');
    }
}
