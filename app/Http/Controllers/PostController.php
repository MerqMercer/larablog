<?php

namespace App\Http\Controllers;

use App\Models\Post;
use App\Models\Tag;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Inertia\Inertia;

class PostController extends Controller
{
    public function create()
    {
        $allTags = Tag::get(['id as value', 'name as label']);
        return Inertia::render('Post/Create', ['allTags' => $allTags]);
    }

    public function store(Request $request)
    {
        $message = 'Post was created!';

        $post = $request->user()->posts()->create($request->validate([
            'title' => 'required|min:3|max:150',
            'content' => 'required',
            'selected_tags' => 'nullable|array',
            'main_picture' => 'nullable|mimes:jpg,png,jpeg,webp|max:5000',
            'thumbnail_picture' => 'nullable|mimes:jpg,png,jpeg,webp|max:5000',
        ],
            [
                'main_picture.mimes' => 'The file should be in one of the formats: jpg, png, jpeg, webp',
                'thumbnail_picture.mimes' => 'The file should be in one of the formats: jpg, png, jpeg, webp'
            ])
        );

        $selectedTags = $request->input('selected_tags');

        if ($selectedTags) {
            $post->tags()->attach($selectedTags);
        }

        if ($request->hasFile('main_picture')) {
            $path = $request->file('main_picture')->store('posts_pictures/' . $post->id, 'public');
            if ($path) {
                $post->update(['picture_path' => $path]);
            } else {
                $message .= "\n Main picture was not uploaded!";
            }
        }

        if ($request->hasFile('thumbnail_picture')) {
            $path = $request->file('thumbnail_picture')->store('posts_pictures/' . $post->id, 'public');
            if ($path) {
                $post->update(['thumbnail_path' => $path]);
            } else {
                $message .= "\n Thumbnail picture was not uploaded!";
            }
        }

        return redirect()->route('user-account.index')
            ->with('success', $message);
    }

    public function show(Post $post)
    {
        $post->load('author');
        return Inertia::render('Post/Show', [
            'post' => $post
        ]);
    }

    public function update(Request $request, Post $post)
    {
        if ($request->user()->cannot('update', $post)) {
            abort(403);
        }

        $message = 'Post was updated!';

        $post->update($request->validate([
            'title' => 'required|min:3|max:150',
            'content' => 'required',
            'selected_tags' => 'nullable|array',
            'main_picture' => 'nullable|mimes:jpg,png,jpeg,webp|max:5000',
            'thumbnail_picture' => 'nullable|mimes:jpg,png,jpeg,webp|max:5000',
        ],
            [
                'main_picture.mimes' => 'The file should be in one of the formats: jpg, png, jpeg, webp',
                'thumbnail_picture.mimes' => 'The file should be in one of the formats: jpg, png, jpeg, webp'
            ])
        );

        $post->tags()->sync($request->input('selected_tags'));

        if ($request->hasFile('main_picture')) {

            if ($post->picture_path) {
                Storage::disk('public')->delete($post->picture_path);
            }

            $path = $request->file('main_picture')->store('posts_pictures/' . $post->id, 'public');

            if ($path) {
                $post->update(['picture_path' => $path]);
            } else {
                $message .= 'Main picture was not updated!';
            }
        }

        if ($request->hasFile('thumbnail_picture')) {

            if ($post->thumbnail_path) {
                Storage::disk('public')->delete($post->thumbnail_path);
            }

            $path = $request->file('thumbnail_picture')->store('posts_pictures/' . $post->id, 'public');

            if ($path) {
                $post->update(['thumbnail_path' => $path]);
            } else {
                $message .= "\n Thumbnail picture was not updated!";
            }
        }

        return redirect()->route('user-account.index')
            ->with('success', $message);

    }

    public function destroy(Post $post, Request $request)
    {
        if ($request->user()->cannot('update', $post)) {
            abort(403);
        }

        if ($post->picture_path) {
            Storage::disk('public')->delete($post->picture_path);
        }

        $post->tags()->detach();
        $post->deleteOrFail();

        return redirect()->route('user-account.index')
            ->with('success', 'Post was deleted');
    }

    public function edit(Post $post, Request $request)
    {
        if ($request->user()->cannot('update', $post)) {
            abort(403);
        }

        $allTags = Tag::get(['id as value', 'name as label']);
        return Inertia::render('Post/Edit', [
            'post' => $post->load('tags:id'),
            'allTags' => $allTags
        ]);
    }
}
