<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Support\Facades\Auth;

class Post extends Model
{
    use HasFactory;

    protected $fillable = [
        'title',
        'content',
        'picture_path',
        'thumbnail_path',
    ];

    protected $sortable = [
        'created_at'
    ];

    public function tags()
    {
        return $this->belongsToMany(Tag::class);
    }

    public function author(): BelongsTo
    {
        return $this->belongsTo(
            \App\Models\User::class,
            'author_id'
        );
    }

    public function scopeMostRecent(Builder $query): Builder
    {
        return $query->orderBy('created_at', 'desc');
    }

    public function scopeByMe(Builder $query): Builder
    {
        return $query->where('author_id', Auth::user()?->id);
    }

    public function scopeSort(Builder $query, array $sort): Builder
    {
        return $query->when(
            $sort['created_at'] ?? 'desc',
            fn($query, $value) => $query->orderBy('created_at', $value)
        );
    }
}
