<?php

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     */
    public function run(): void
    {
        \App\Models\User::factory()->create([
            'name' => 'Test User',
            'email' => 'test@gmail.com',
            'is_admin' => true,

        ]);

        $users = \App\Models\User::factory(10)->create();

        $users->each(function (\App\Models\User $user) {
            $posts = \App\Models\Post::factory(6)
                ->create(['author_id' => $user->id]);

            $tags = \App\Models\Tag::factory(6)->create();

            $tags->each(function (\App\Models\Tag $tag) use ($posts) {

                $tag->posts()->saveMany($posts);
            });

        });

    }
}
