<?php

namespace Database\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;

/**
 * @extends \Illuminate\Database\Eloquent\Factories\Factory<\App\Models\Model>
 */
class PostFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    public function definition(): array
    {
        $date = fake()->dateTimeThisYear();
        return [
            'title' => fake()->sentence(12),
            'content' => fake()->paragraph(50),
            'author_id' => \App\Models\User::factory(),
            'created_at' => $date,
        ];
    }
}
